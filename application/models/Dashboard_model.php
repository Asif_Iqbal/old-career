<?php
/**
 * Created by PhpStorm.
 * User: sultan
 * Date: 2/7/19
 * Time: 11:16 AM
 */
    class Dashboard_model extends CI_Model{

        function __construct(){
            parent::__construct();
            $this->load->database();
        }

        function getPosts(){
            $query = $this->db->get('posts');
            return $query->result();
        }


            function getUserName($uid){
                $this->db->select('*');
                $this->db->from('users');
                $this->db->where('id', $uid);

                $query = $this->db->get();

                if($query->num_rows() == 1 ){
                    return $query->result();
                } else{
                    return false;
                }

            }

        function addData(){
            $data = array(
                'u_id' => $this->input->post('u_id'),
                'title' => $this->input->post('title'),
                'body' => $this->input->post('body')
            );

            return $this->db->insert('posts', $data);

        }

        function countPosts(){
            return $this->db->count_all('posts');
        }

        function countUsers(){
            return $this->db->count_all('users');
        }

        function getBlogById($id){
            $this->db->where('id', $id);
            $query = $this->db->get('posts');
            if($query->num_rows() > 0){
                return $query->row();
            } else{
                return false;
            }
        }

        function d_post($id){

            $this->db->where('id', $id);
            $this->db->delete('posts');
            if($this->db->affected_rows()>0){
                return true;
            } else{
                return false;
            }
        }

        function update(){
            $id = $this->input->post('text_hidden');
            $field = array(
                'title' => $this->input->post('title'),
                'body' => $this->input->post('body'),
                'updated_on' => date('Y-m-d H:i:s')
            );
            $this->db->where('id', $id);
            $this->db->update('posts', $field);
            if($this->db->affected_rows() > 0){
                return true;
            } else{
                return false;
            }
        }

        function comment($data){
            return $this->db->insert('comments', $data);
        }

        function getComments($id){
            $this->db->select('*');
            $this->db->where('c_blog_id', $id);
            $query = $this->db->get('comments');
            if($query->num_rows() > 0){
                return $query->result();
            } else{
                return false;
            }
        }

        function update_comment(){
            $id = $this->input->post('c_id');
            $comment = $this->input->post('update_comment');

            $data = array('comment' => $comment);
            
            $this->db->where('id', $id);
            $this->db->update('comments', $data);
            if($this->db->affected_rows() > 0){
                return true;
            } else{
                return false;
            }
        }

        function d_comment(){
            $id = $this->input->post('post_id');

            $this->db->where('id', $id);
            $this->db->delete('comments');
            if($this->db->affected_rows()>0){
                return true;
            } else{
                return false;
            }
        }
    }
