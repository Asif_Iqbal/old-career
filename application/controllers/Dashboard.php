<?php

	class Dashboard extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->model('dashboard_model');
		}

		function index(){
			if($this->session->userdata('logged')){
			    $posts['blogs'] = $this->dashboard_model->getPosts();
                $posts['count_posts'] = $this->dashboard_model->countPosts();
                $posts['count_users'] = $this->dashboard_model->countUsers();
                $posts['created_by'] = $this->dashboard_model->getPosts();

                $data = $posts['blogs'];

                for ($i = 0; $i < count($data); $i++){
                    $uid = $data[$i]->u_id;
                    $name = $this->dashboard_model->getUserName($uid);
                    $data[$i]->username = $name[0]->username;
                }

			    $this->load->view('dashboard/dashboard_head');
				$this->load->view('dashboard/dashboard', $posts);
				$this->load->view('dashboard/dashboard_foot');
			} else{
				redirect('welcome');
			}
		}


		function logout(){
		$this->session->unset_userdata('id');
			$this->session->sess_destroy();
			redirect('welcome');
		}

		function getpost(){
		    $this->load->view('posts/get');
        }

		function addpost(){
		    $this->load->view('dashboard/dashboard_head');
		    $this->load->view('posts/add');
		    $this->load->view('dashboard/dashboard_foot');
        }

        function add(){

		    $add_data = $this->dashboard_model->addData();

		    if($add_data){
		        redirect('dashboard');
            } else{
		        redirect('dashboard/addpost');
            }
        }

        function view($id){
        	$data['blog'] = $this->dashboard_model->getBlogById($id);
        	$data['comment'] = $this->dashboard_model->getComments($id);
        	
        	$value = $data['comment'];

        	if($value != null){
        		for ($i = 0; $i < count($value); $i++){
                    $uid = $value[$i]->c_user_id;
                    $name = $this->dashboard_model->getUserName($uid);
                    $value[$i]->username = $name[0]->username;
                }
        	}

        	// $value[2]->address = "Uttara";
        	// echo "<pre>";
        	// print_r($value);
        	// echo "</pre>";
        	// exit();


        	$this->load->view('dashboard/dashboard_head');
        	$this->load->view('dashboard/view', $data);
        	$this->load->view('dashboard/dashboard_foot');
        }

        function editpost($id){
        	$data['blog'] = $this->dashboard_model->getBlogById($id);
        	$this->load->view('dashboard/dashboard_head');
		    $this->load->view('posts/edit', $data);
		    $this->load->view('dashboard/dashboard_foot');
        }

        function deletepost($id){
            $del = $this->dashboard_model->d_post($id);
            if($del){
                redirect('dashboard');
            } else{
                redirect('dashboard');
            }
        }

        function update(){
        	$result = $this->dashboard_model->update();
        	if($result){
        		redirect('dashboard');
        	} else{
        		redirect('dashboard/editpost');
        	}
        }

        function comment(){
        	$data = array(
        		'c_blog_id' => $this->input->post('c_blog_id'),
        		'c_user_id' => $this->input->post('c_user_id'),
        		'comment' => $this->input->post('comment')
        	);

        	$var = $this->input->post('c_blog_id');

        	$comment['data'] = $this->dashboard_model->comment($data);
        	redirect('dashboard/view/'.$var);
        }

        function edit_comment($p_id){

            $var = $this->dashboard_model->update_comment();
            if($var){
                redirect('dashboard/view/'.$p_id);
            } else{
                redirect('dashboard/view/'.$p_id);
            }
        }

        function delete_comment($c_id){
            $del = $this->dashboard_model->d_comment();
            if($del){
                redirect('dashboard/view/'.$c_id);
            } else{
                redirect('dashboard/view/'.$c_id);
            }
        }

	}

?>