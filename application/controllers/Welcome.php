<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model(array('welcome_model'));
	}

	
	function index(){
		$this->load->view('sections/index');
	}

	function computer(){
		$this->load->view('sections/header');
		$this->load->view('sections/computer');
		$this->load->view('sections/footer');
	}

	function science(){
		$this->load->view('sections/header');
		$this->load->view('sections/science');
		$this->load->view('sections/footer');
	}

	function engineer(){
		$this->load->view('sections/header');
		$this->load->view('sections/engineer');
		$this->load->view('sections/footer');
	}

	function skillsharing(){
		$this->load->view('sections/header');
		$this->load->view('sections/skillsharing');
		$this->load->view('sections/footer');
	}

	function software(){
		$this->load->view('sections/header');
		$this->load->view('sections/software');
		$this->load->view('sections/footer');
	}

	function query(){
		$this->load->view('query/check');
	}

	function query_check(){
		//FETCHING CATEGORY TABLE AS ARRAY OF OBJECTS
		/*Array
		(
		    [0] => Array
		        (
		            [c_id] => 1
		            [c_name] => web
		        )

		    [1] => Array
		        (
		            [c_id] => 2
		            [c_name] => design
		        )

		    [2] => Array
		        (
		            [c_id] => 3
		            [c_name] => desktop
		        )

		)*/
		$data = $this->welcome_model->exam_result();
		//END===================


		//FETCHING DATA FROM FORM AS ARRAY
		/*Array
		(
		    [html] => html
		    [js] => js
		)*/
		 $num = $this->input->post();
		 //END====================

		 

		 //FETCHING SKILL.ID FROM SKILL TABLE USING SKILL.NAME DYNAMICALLY BY FOREACH()
		 //AFTER FETCHING $NUM ARRAY CHANGE AS LIKE BELOW COMMENTED ARRAY:
		 /*Array
		(
		    [html] => 2
		    [js] => 3
		)*/
		 $var = $num;
		 foreach($var as $v){

		 	$this->load->database();
			$this->db->select('s_id');
			$this->db->from('skills');
			$this->db->where('s_name', $v);

			$query = $this->db->get();
			$da = $query->result();

			$num[$v]= $da[0]->s_id;
		 };
		 //END==============



		 //GLOBAL VARIABLE
		 $plus = 0;
		 
		 //FETCHING VALUES FROM VALUE BASED ON CATEGORY ID & SKILL ID;
		 for($i=0; $i<count($data); $i++){ //TO FETCH SKILL VALUES TO ASSIGN IN $data['all 3 indexeds']['skills'] ARRAY
			foreach($num as $v){ //GETTING SKILL VALUE OF ALL THE SKILLS('html', 'js') FOR EVERY SINGLE CATEGORY;
			 	 $this->db->select('value');
				 $this->db->from('value');
				 $this->db->where('c_id', $data[$i]['c_id']);
				 $this->db->where('s_id', $v);
				 $query = $this->db->get();

				 $val = $query->result();

				 $plus += $val[0]->value; //ASSIGNING & SUMMATION OF SKILL VALUES;
				
			 } //END FOREACH;

		 	$data[$i]['skills'] = $plus; //ASSIGNING THE WHOLE PERCENTIGE  VALUE TO THE CATEGORIES('web','desktop','design');
		 	$plus = 0; //re initiating plus as 0 because after the foreach executed it means $plus must be starts from 0 to have the absolute value for the next category.
		 };


		 
		 $values['key'] = $data;
		 $this->load->view('query/data', $values);
		
	}

	


}
