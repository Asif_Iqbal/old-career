<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Query_Test extends CI_Migration {

	public function up()
	{
		//CREATE SKILL TABLE
		$this->dbforge->add_field(array(
			's_id' => array(
				'type' => 'INT',
				'constraint' => 5,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			's_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
			)
		));
		$this->dbforge->add_key('s_id', TRUE);
		$this->dbforge->create_table('skills');

		//CREATE CATEGORY TABLE
		$this->dbforge->add_field(array(
			'c_id' => array(
				'type' => 'INT',
				'constraint' => 5,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'c_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 30,
			)
		));
		$this->dbforge->add_key('c_id', TRUE);
		$this->dbforge->create_table('category');

		//CREATE VALUE TABLE
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 5,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			's_id' => array(
				'type' => 'INT',
				'constraint' => 5,
			),
			'c_id' => array(
				'type' => 'INT',
				'constraint' => 5,
			),
			'value' => array(
				'type' => 'INT',
				'constraint' => 30
			)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('value');

		//INSERT skills
		$data = array(
				array('s_name' => "php"),
				array('s_name' => "html"),
				array('s_name' => "js"),
				array('s_name' => "java"),
				array('s_name' => "flash"),
				array('s_name' => "sql")
			);
		$this->db->insert_batch('skills', $data);


		//INSERT category
		$data = array(
				array('c_name' => "web"),
				array('c_name' => "design"),
				array('c_name' => "desktop")
			);
		$this->db->insert_batch('category', $data);
	

		//INSERT value
		$data = array(
				array('s_id' => 1, 'c_id' => 1, 'value' => 10),
				array('s_id' => 1, 'c_id' => 2, 'value' => 0),
				array('s_id' => 1, 'c_id' => 3, 'value' => 20),
				array('s_id' => 2, 'c_id' => 1, 'value' => 10),
				array('s_id' => 2, 'c_id' => 2, 'value' => 30),
				array('s_id' => 2, 'c_id' => 3, 'value' => 0),
				array('s_id' => 3, 'c_id' => 1, 'value' => 20),
				array('s_id' => 3, 'c_id' => 2, 'value' => 50),
				array('s_id' => 3, 'c_id' => 3, 'value' => 0),
				array('s_id' => 4, 'c_id' => 1, 'value' => 10),
				array('s_id' => 4, 'c_id' => 2, 'value' => 20),
				array('s_id' => 4, 'c_id' => 3, 'value' => 20),
				array('s_id' => 5, 'c_id' => 1, 'value' => 10),
				array('s_id' => 5, 'c_id' => 2, 'value' => 0),
				array('s_id' => 5, 'c_id' => 3, 'value' => 20),
				array('s_id' => 6, 'c_id' => 1, 'value' => 30),
				array('s_id' => 6, 'c_id' => 2, 'value' => 0),
				array('s_id' => 6, 'c_id' => 3, 'value' => 40)
				// array('s_id' => 1, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 1, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 2, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 3, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 4, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 5, 'c_id' => 3, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 1, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 2, 'value' => 10),
				// array('s_id' => 6, 'c_id' => 3, 'value' => 10),
			);
		$this->db->insert_batch('value', $data);
	}

	public function down()
	{
		//DROP TABLES
		$this->dbforge->drop_table('skills');
		$this->dbforge->drop_table('category');
		$this->dbforge->drop_table('value');
	}
}
?>