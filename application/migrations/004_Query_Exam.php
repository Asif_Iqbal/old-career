<?php

	class Migration_Query_Exam extends CI_Migration{

		public function up(){
			$this->load->dbforge();

			$this->dbforge->add_field(
				array(
					'id' => array(
						'type' => 'INT',
						'constant' => 5,
						'unsigned' => TRUE,
            			'auto_increment' => TRUE
					),
					'name'=>array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'value' => array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					)
				)
			);
			$this->dbforge->add_key('id', TRUE);
			$this->dbforge->create_table('exam');

		}

		public function down(){
			$this->load->dbforge();
			$this->dbforge->drop_table('exam');
		}
	}

?>