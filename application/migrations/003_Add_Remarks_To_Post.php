<?php
/**
 * Created by PhpStorm.
 * User: sultan
 * Date: 2/7/19
 * Time: 11:35 AM
 */
    class Migration_Add_Remarks_To_Post extends CI_Migration{

       public function up(){
            $this->load->dbforge();

            $this->dbforge->add_field(
                array(
                    'id' => array(
                        'type' => 'INT',
                        'constant' => 5,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'c_blog_id'=>array(
                        'type' => 'INT',
                        'constraint' => '5'
                    ),
                    'c_user_id' => array(
                        'type' => 'INT',
                        'constraint' => '5'
                    ),
                    'comment' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100'
                    )
                )
            );
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('comments');

        }

        public function down(){
            $this->dbforge->drop_table('comments');
        }
    }