<?php
/**
 * Created by PhpStorm.
 * User: sultan
 * Date: 2/7/19
 * Time: 11:35 AM
 */
    class Migration_Add_post extends CI_Migration{

        public function up(){
            $this->load->dbforge();

            $this->dbforge->add_field(
                array(
                    'id' => array(
                        'type' => 'INT',
                        'constant' => 5,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                    ),
                    'title'=>array(
                        'type' => 'VARCHAR',
                        'constraint' => '100'
                    ),
                    'body' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '255'
                    ),
                    'u_id' => array(
                        'type' => 'INT',
                        'constraint' => '11'
                    ),
                    'created_on' => array(
                        'type' =>  'TIMESTAMP'
                    ),
                    'updated_on' => array(
                        'type' => 'TIMESTAMP'
                    )
                )
            );
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('posts');
        }

        public function down(){
            $this->load->dbforge();
            $this->dbforge->drop_table('posts');
        }
    }