<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">



    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url().'assets/bootstrap/css/bootstrap.css'; ?>">
    <link rel="stylesheet" href="<?php echo base_url().'assets/bootstrap/css/style.css'; ?>">
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
</head>

<body>

<nav class="navbar navbar-toggleable-sm navbar-inverse bg-inverse p-0 clearfix">
    <div class="container clearfix" style="width: 80%;">
        <a href="<?php echo base_url().'dashboard'; ?>" class="navbar-brand">BLOG</a>
        <div class="collapse navbar-collapse" id="mynavbar" style="height: auto">

            <ul class="navbar-nav ml-auto">

                <li class="=nav-item dropdown mr-3">
                    <a href="#" class="nav-link" dropdown-toggle data-toggle="dropdown">
                        <i class= "fa fa-user"> </i>
                        <?php if($this->session->userdata('logged')): ?>
                            <?php echo $this->session->userdata('logged'); ?>
                        <?php endif; ?>
                    </a>


                </li>

                <li class="nav-item">
                    <a href="<?php echo base_url().'dashboard/logout'; ?>" class="nav-link"><i class= "fa fa-user-times"></i>Logout</a>
                </li>
            </ul>
        </div>
    </div>
</nav>