<div>
    <h3>Add Blog</h3>
    <a href="<?php echo base_url().'dashboard'; ?>" class="btn btn-primary">Back</a>
    <form action="<?php echo base_url().'dashboard/update'; ?>" method="post" class="form-horizontal">
    	<input type="hidden" name="text_hidden" value="<?php echo $blog->id; ?>">
        <div class="form-group">
            <label for="title" class="col-md-2 text-right">Title</label>
            <div class="col-md-10">
                <input type="text" name="title" value="<?php echo $blog->title; ?>" class="form-control" required>
            </div>
        </div>
        <div class="form-group">
            <label for="description" class="col-md-2 text-right">Description</label>
            <div class="col-md-10">
                <textarea name="body" class="form-control"><?php echo $blog->body; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 text-right"></label>
            <div class="col-md-10">
                <input type="submit" name="btnSave" class="btn btn-primary" value="Update">
            </div>
        </div>
    </form>
</div>