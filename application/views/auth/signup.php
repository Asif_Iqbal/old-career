<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/style3.css') ?>">
<div class="header">
    <h2>Registration</h2>
</div>	
  <form method="post" action="<?= base_url().'auth/register'; ?>">
  
  	<div class="input-group">
  	  <label>Username</label>
  	  <input type="text" name="username" placeholder="Enter Username">
  	</div>
  	<div class="input-group">
  	  <label>Email</label>
  	  <input type="email" name="email" >
  	</div>
  	<div class="input-group">
  	  <label>Password</label>
  	  <input type="password" name="password">
  	</div>
  	<div class="input-group">
  	  <label>Confirm password</label>
  	  <input type="password" name="c_password">
  	</div>
  	<div class="input-group">
  	  <button type="submit" class="btn modify-btn" name="reg_user">Register</button>
  	</div>
  	<p>
  		Already a member? <a href="<?= base_url('auth/login')?>">Sign in</a>
  	</p>
  </form>