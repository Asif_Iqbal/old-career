    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/style3.css'; ?>">

    <div>
      <?php if($this->session->flashdata('success')): ?>
        <div class="col-md-8 col-md-offset-2">
          <div class="alert alert-success">
          <?php echo $this->session->flashdata('success'); ?>
         </div>
        </div>
      <?php endif; ?>
    </div>

    <div>
      <?php if($this->session->flashdata('invalid')): ?>
        <div class="col-md-8 col-md-offset-2">
          <div class="alert alert-danger">
          <?php echo $this->session->flashdata('invalid'); ?>
         </div>
        </div>
      <?php endif; ?>
    </div>

  	<div class="header">
  	    <h2>Login</h2>
  	</div>
	 
  <form method="post" action="<?php echo base_url().'auth/logged'; ?>">
  	<div class="input-group">
  		<label>Email</label>
  		<input type="text" name="email" >
  	</div>
  	<div class="input-group">
  		<label>Password</label>
  		<input type="password" name="password">
  	</div>
  	<div class="input-group">
  		<button type="submit" class="btn modify-btn" name="login_user">Login</button>
  	</div>
  	<p>
  		Not yet a member? <a href= "<?= base_url('auth/signup')?>">Registration</a>
  	</p>
  </form>

  <br>
  <br>
  <br>







