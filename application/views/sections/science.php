<link href="<?php echo base_url('assets/css/science.css'); ?>" rel="stylesheet" media="screen">
<section>

            <div class="sci-life"> Welcome<br>
                To Science world
            </div>
        </section>
        <br>


        <section id="video">

            <div class="contant">
                <div class="col-sm-6">

                    <video class="video1" controls>
                        <source src="<?php echo base_url('assets/videos/CAREER%20OPTIONS%20IN%20SCIENCE%20%E2%80%93%202.MP4'); ?>" type="video/mp4">

                    </video>

                    <div class="video-title">
                        <h3>Discover Your career</h3>
                    </div>

                </div>

                <div class="col-sm-6 sci-left">

                    <div class="title">
                        <h1>What is Science?</h1>
                    </div>
                    <div class="details">
                        <p> Science is the concerted human effort to understand, or to understand better, the history
                            of the natural world and how the natural world works, with observable physical evidence as
                            the basis of that understanding1. It is done through observation of natural phenomena,
                            and/or through experimentation that tries to simulate natural processes under controlled
                            conditions.</p>

                    </div>
                    <div class="title">
                        <h1>Why do Science? </h1>
                    </div>
                    <div class="details">
                        <p> So why are all these people described above doing what they're doing? In most cases,
                            they're collecting information to test new ideas or to disprove old ones. Scientists become
                            famous for discovering new things that change how we think about nature, whether the
                            discovery is a new species of dinosaur or a new way in which atoms bond. Many scientists
                            find their greatest joy in a previously unknown fact (a discovery) that explains something
                            problem previously not explained, or that overturns some previously accepted idea..</p>

                    </div>



                </div>
            </div>



        </section>



        <section>

            <div class="sci-life"> Discover your<br>
                Path in Science
            </div>
            <div class="col-sm-4 videos">

                <video class="video2" controls>
                    <source src="<?php echo base_url('assets/videos/Engineering.MP4'); ?>" type="video/mp4">

                </video>
                <p class="choise">Engineer Careers!!</p>
                <a href="<?php echo base_url('welcome/engineer'); ?>" class="btn">Discover more</a>


            </div>
            <div class="col-sm-4 videos">

                <video class="video2" controls>
                    <source src="<?php echo base_url('assets/videos/MBBS.MP4'); ?>" type="video/mp4">
                </video>
                <p class="choise">MBBS Careers!!</p>
                <a href="#" class="btn">Discover more</a>


            </div>
            <div class="col-sm-4 videos">

                <video class="video2" controls>
                    <source src="<?php echo base_url('assets/videos/CAREER%20OPTIONS%20IN%20SCIENCE%20%E2%80%93%202.MP4'); ?>" type="video/mp4">
                </video>
                <p class="choise">Genarel Career!!.</p>
                <a href="#" class="btn">Discover more</a>


            </div>


        </section>


        <section>
            <div class="col-sm-4 videos">

                <video class="video2" controls>
                    <source src="<?php echo base_url('assets/videos/CAREER%20OPTIONS%20IN%20SCIENCE%20%E2%80%93%202.MP4'); ?>" type="video/mp4">
                </video>
                <p class="choise">Your choise.</p>
                <a href="#" class="btn">Discover more</a>


            </div>
            <div class="col-sm-4 videos">



                <video class="video2" controls>
                    <source src="<?php echo base_url('assets/videos/CAREER%20OPTIONS%20IN%20SCIENCE%20%E2%80%93%202.MP4'); ?>" type="video/mp4">
                </video>
                <p class="choise">Your choise.</p>

                <a href="#" class="btn">Discover more</a>



            </div>
            <div class="col-sm-4 videos">

                <video class="video2" controls>
                    <source src="<?php echo base_url('assets/videos/CAREER%20OPTIONS%20IN%20SCIENCE%20%E2%80%93%202.MP4'); ?>" type="video/mp4">
                </video>
                <p class="choise">Your choise.</p>
                <a href="#" class="btn">Discover more</a>


            </div>


        </section>
        <br><br>