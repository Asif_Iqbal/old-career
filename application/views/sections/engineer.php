<link href="<?php echo base_url('assets/css/engineer.css'); ?>" rel="stylesheet" media="screen">
<section>

            <div class="sci-life"> Welcome<br>
                To Engineering Career!
            </div>
        </section>
        <br><br>

        <section id="video">

            <div class="contant">
                <div class="col-sm-6">


                    <video class="video1" controls>
                        <source src="<?php echo base_url('assets/videos/Engineering.MP4'); ?>" type="video/mp4">


                    </video>



                    <div class="video-title">
                        <h3>Discover Your career</h3>
                    </div>

                </div>

                <div class="col-sm-6">
                    <div class="topic">
                        <h3>popular catagory</h3>


                        <ul>

                            <li>
                                <a href="#" onclick="document.getElementById('id01').style.display='block'" class="w3">Computer
                                    Engineer</a>

                                <div id="id01" class="w3-modal">
                                    <div class="w3-modal-content">
                                        <header class="w3-container w3-black">
                                            <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-display-topright">&times;</span>
                                            <h2>Engineering</h2>
                                        </header>
                                        <div class="w3-container">
                                            <a href="#">cse </a>
                                            <br>
                                            <a href="#">cse </a>
                                            <br>
                                            <a href="#">cse </a>
                                            <br>
                                            <a href="#">cse </a>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <a href="#" onclick="document.getElementById('id01').style.display='block'" class="w3">Electrical
                                    Engineer</a>

                                <div id="id01" class="w3-modal">
                                    <div class="w3-modal-content">
                                        <header class="w3-container w3-black">
                                            <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-display-topright">&times;</span>
                                            <h2>Engineering</h2>
                                        </header>
                                        <div class="w3-container">
                                            <a href="#">cse </a>
                                            <br>
                                            <a href="#">cse </a>
                                            <br>
                                            <a href="#">cse </a>
                                            <br>
                                            <a href="#">cse </a>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <a href="#" onclick="document.getElementById('id01').style.display='block'" class="w3">Environmental
                                    Engineer</a>

                                <div id="id01" class="w3-modal">
                                    <div class="w3-modal-content">
                                        <header class="w3-container w3-black">
                                            <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-display-topright">&times;</span>
                                            <h2>Engineering</h2>
                                        </header>
                                        <div class="w3-container">
                                            <a href="#">cse </a>
                                            <br>
                                            <a href="#">cse </a>
                                            <br>
                                            <a href="#">cse </a>
                                            <br>
                                            <a href="#">cse </a>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <a href="#" onclick="document.getElementById('id01').style.display='block'" class="w3">Mechanical
                                    Engineer</a>

                                <div id="id01" class="w3-modal">
                                    <div class="w3-modal-content">
                                        <header class="w3-container w3-black">
                                            <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-display-topright">&times;</span>
                                            <h2>Engineering</h2>
                                        </header>
                                        <div class="w3-container">
                                            <a href="#">cse </a>
                                            <br>
                                            <a href="#">cse </a>
                                            <br>
                                            <a href="#">cse </a>
                                            <br>
                                            <a href="#">cse </a>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <a href="#" onclick="document.getElementById('id01').style.display='block'" class="w3">Software
                                    Engineer</a>

                                <div id="id01" class="w3-modal">
                                    <div class="w3-modal-content">
                                        <header class="w3-container w3-black">
                                            <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-display-topright">&times;</span>
                                            <h2>Engineering</h2>
                                        </header>
                                        <div class="w3-container">
                                            <a href="#">cse </a>
                                            <br>
                                            <a href="#">cse </a>
                                            <br>
                                            <a href="#">cse </a>
                                            <br>
                                            <a href="#">cse </a>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </li>

                        </ul>

                        <div>

                            <a href="#popup" class="btn btn-click">view all</a>
                        </div>


                    </div>
                </div>

            </div>


        </section>

        <section>

            <div class="sci-life"> Discover your<br>
                Path in Engineering!
            </div>
            <div class="col-sm-4 videos">

                <video class="video2" controls>
                    <source src="<?php echo base_url('assets/videos/Computer Science.MP4'); ?>" type="video/mp4">

                </video>
                <p class="choise">COMPUTER ENGINEER!</p>
                <a href="<?php echo base_url('welcome/computer'); ?>" class="btn1">Discover more</a>


            </div>
            <div class="col-sm-4 videos">

                <video class="video2" controls>
                    <source src="<?php echo base_url('assets/videos/MBBS.MP4'); ?>" type="video/mp4">
                </video>
                <p class="choise">ELECTRICAL ENGINEER!</p>
                <a href="#" class="btn1">Discover more</a>


            </div>
            <div class="col-sm-4 videos">

                <video class="video2" controls>
                    <source src="<?php echo base_url('assets/videos/CAREER%20OPTIONS%20IN%20SCIENCE%20%E2%80%93%202.MP4'); ?>" type="video/mp4">
                </video>
                <p class="choise">MECHANICAL ENGINEER!.</p>
                <a href="#" class="btn1">Discover more</a>


            </div>

        </section>