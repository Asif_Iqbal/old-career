<section class="footer1">


            <!-- Footer start -->

            <footer id="footer">
                <div class="container2">

                    <ul class="social-links">
                        <li>
                            <a href="https://www.facebook.com" target="_blank" class="wow fadeInUp">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com" target="_blank" class="wow fadeInUp" data-wow-delay=".1s">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://plus.google.com" target="_blank" class="wow fadeInUp" data-wow-delay=".2s">
                                <i class="fa fa-google-plus"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.pinterest.com" target="_blank" class="wow fadeInUp" data-wow-delay=".4s">
                                <i class="fa fa-pinterest"></i>
                            </a>
                        </li>
                    </ul>
                </div>

                <!-- .row -->

                <!-- .container -->
            </footer>

            <!-- Footer end -->


        </section>




    </body>

</html>
