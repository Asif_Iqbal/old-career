<!-- Custom styles CSS -->
        <link href="<?php echo base_url('assets/css/computer.css'); ?>" rel="stylesheet" media="screen">
<section>

            <div class="sci-life"> Welcome<br>
                To Computer Engineering world
            </div>
        </section>
        <br>


        <section id="video">

            <div class="contant">
                <div class="col-sm-6">

                    <video class="video1" controls>
                        <source src="<?php echo base_url('assets/videos/Computer Science.MP4'); ?>" type="video/mp4">

                    </video>

                    <div class="video-title">
                        <h3>Discover Your career</h3>
                    </div>

                </div>

                <div class="col-sm-6 sci-left">

                    <div class="title">
                        <h1>What is computer <br>Engineering all about?</h1>
                    </div>
                    <div class="details">
                        <p> Computer Engineering is a discipline which resides at the intersection of Computer Science
                            and Electrical Engineering. Computer Engineers are often described as Electrical Engineers
                            with specific training in computer hardware and in the interaction between hardware and
                            software. Examples of the products created and developed by Computer Engineers are the
                            mobile phone and the various play stations and computer and video games that have become so
                            popular in the last 10-15 years.

                            Some of the fields that are unique to Computer Engineering include design of Very Large
                            Scale Integrated (VLSI) systems for computing hardware, and of essential components of
                            computers such as memories and electronic circuitry, including analog circuits and digital
                            hardware. Recently the design and operation of computer networks and communication networks
                            (such as the Internet) have become an integral part of Computer Engineering..</p>

                    </div>




                </div>
            </div>



        </section>



        <section>

            <div class="sci-life"> Discover your<br>
                Path in Computer Engineering!
            </div>
            <div class="col-sm-4 videos">

                <video class="video2" controls>
                    <source src="<?php echo base_url('assets/videos/Engineering.MP4'); ?>" type="video/mp4">

                </video>
                <p class="choise">Web Developers!!</p>
                <a href="<?php echo base_url('welcome/software'); ?>" class="btn">Discover more</a>


            </div>
            <div class="col-sm-4 videos">

                <video class="video2" controls>
                    <source src="<?php echo base_url('assets/videos/MBBS.MP4'); ?>" type="video/mp4">
                </video>
                <p class="choise">Software Developer!!</p>
                <a href="#" class="btn">Discover more</a>


            </div>
            <div class="col-sm-4 videos">

                <video class="video2" controls>
                    <source src="<?php echo base_url('assets/videos/CAREER%20OPTIONS%20IN%20SCIENCE%20%E2%80%93%202.MP4'); ?>" type="video/mp4">
                </video>
                <p class="choise">Android Developer!!.</p>
                <a href="#" class="btn">Discover more</a>


            </div>


        </section>


        <section>
            <div class="col-sm-4 videos">

                <video class="video2" controls>
                    <source src="<?php echo base_url('assets/videos/CAREER%20OPTIONS%20IN%20SCIENCE%20%E2%80%93%202.MP4'); ?>" type="video/mp4">
                </video>
                <p class="choise">Your choise.</p>
                <a href="#" class="btn">Discover more</a>


            </div>
            <div class="col-sm-4 videos">



                <video class="video2" controls>
                    <source src="<?php echo base_url('assets/videos/CAREER%20OPTIONS%20IN%20SCIENCE%20%E2%80%93%202.MP4'); ?>" type="video/mp4">
                </video>
                <p class="choise">Your choise.</p>

                <a href="#" class="btn">Discover more</a>



            </div>
            <div class="col-sm-4 videos">

                <video class="video2" controls>
                    <source src="<?php echo base_url('assets/videos/CAREER%20OPTIONS%20IN%20SCIENCE%20%E2%80%93%202.MP4'); ?>" type="video/mp4">
                </video>
                <p class="choise">Your choise.</p>
                <a href="#" class="btn">Discover more</a>


            </div>


        </section>
        <br><br>