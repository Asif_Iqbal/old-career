<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Computer Science</title>

        <!-- CSS -->
        <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('assets/css/simple-line-icons.css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('assets/css/animate.css'); ?>" rel="stylesheet">
        <link rel="shortcut icon" type="image/png" href="<?php echo base_url('assets/images/log.PNG'); ?>">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

        

        <script src="<?php echo base_url('assets/js/modernizr.custom.js'); ?>"></script>

    </head>

    <body>

        <!-- Navigation start -->

        <header class="header">

            <nav class="navbar navbar-custom" role="navigation">

                <div class="container">

                    <div class="navbar-header">

                        <a class="navbar-brand" href="<?php echo base_url(); ?>">Explore Career</a>
                    </div>

                    <div class="collapse navbar-collapse" id="custom-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="<?php echo base_url(); ?>">Home</a>
                            </li>
                            

                            <li>
                                <?php if(!$this->session->userdata('logged')): ?>
                                <a href="<?php echo base_url().'auth/login'; ?>">View More</a>
                            <?php endif; ?>
                            </li>

                            <li>
                                <?php if($this->session->userdata('user')): ?>
                                <a href="<?php echo base_url().'auth/login'; ?>"><?php echo $this->session->userdata('logged'); ?></a>
                            <?php endif; ?>
                            </li>

                            <li>
                                <?php if($this->session->userdata('logged')): ?>
                                <a href="<?php echo base_url().'dashboard'; ?>">Q/A</a>
                            <?php endif; ?>
                            </li>

                            <li>
                                <a href="#">About us</a>
                            </li>

                            <li>
                                <?php if($this->session->userdata('logged')): ?>
                                <a href="<?php echo base_url().'auth/logout'; ?>">Logout</a>
                            <?php endif; ?>
                            </li>

                        </ul>
                    </div>

                </div>
                <!-- .container -->

            </nav>

        </header>
        <br>

        <!-- Navigation end -->